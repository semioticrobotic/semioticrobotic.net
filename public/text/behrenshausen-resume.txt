Bryan G. Behrenshausen, PhD
West Lawn, PA (USA)
bryan@semioticrobotic.net
semioticrobotic.net
github.com/semioticrobotic
(Last updated 2025-02-13)

Open Source Program & Community Management Experience
=====================================================

2024--present
Open Source Program Manager
Open Source Program Office, SAS Institute Inc.

- Designed and executed open source and InnerSource project strategy
- Developed and delivered open source training materials and enablement workshops
- Co-constructed and managed internal open source contributions processes

2022--2024
Senior Open Source Program Manager
Developer Relations, GitLab

- Managed GitLab for Open Source and GitLab Open Source Partners programs
- Wrote and edited public-facing marketing materials, including blog posts and case studies
- Served as liaison to high-profile open source partner projects (including GNOME, KDE, Debian, and Drupal)
- Oversaw open source consortium memberships and event sponsorships
- Maintained program documentation with docs-as-code workflows
- Trained team members in remote/hybrid work best practices (via TeamOps | https://handbook.gitlab.com/teamops/)

2019--2022
Principal Community Architect
Open Source Program Office, Red Hat, Raleigh, NC

- Developed open source and open culture educational, sales enablement, and training materials
- Edited and curated materials for office's resource library
- Managed upstream, global open source community (the Open Organization project)
- Contribute to open source community architecture handbook (the Open Source Way)
- Served as member of The Document Foundation advisory board

2018--2019
Writer, editor, community manager (Corporate Communications)
Marketing Communications + Brand, Red Hat, Raleigh, NC

- Led corporate marketing initiative related to organizational culture
- Served as ghostwriter (articles and presentations) on issues of open source culture for C-suite executives
- Organized and executed community conference activities
- Edited and curated open culture articles for Opensource.com
- Organized and edited community-driven Open Organization book series

2012--2018
Writer, editor, community manager (Opensource.com)
Digital Communities, Red Hat, Raleigh, NC

- Composed original writing on open source technology and culture for Opensource.com
- Managed global volunteer writing community
- Curated and published newsletter
- Edited written materials
- Published articles using Drupal CMS
- Hosted monthly community meetings and Twitter chats
- Managed social media account (Twitter)
- Organized and edited community-driven book series

2011
Brand Marketing intern (Opensource.com)
Brand Marketing, Red Hat, Raleigh, NC

- Composed original writing on open source technology and culture for Opensource.com
- Managed online community via social media
- Recruited volunteer staff writers
- Authored best practices for increasing website traffic using social media
- Assisted with SEO initiatives

2001--2002 & 2005  
Reporter (General Assignment and Features)
Reading Eagle newspaper, Reading, PA

- Worked as daily city/metro reporter
- Composed lifestyle features, fitness pieces, personality profiles, entertainment (book, concert) reviews, and culture pieces
- Coordinated story packages and layout/art for stories
- Edited page proofs for print publication

Instructional Experience
========================

2021
Instructor
Open Source Technology Management, Brandeis University, Waltham, MA

- Developed and instructed courses in open source business strategy and community architecture

2017--2018
Instructor
Innovation & Entrepreneurship, Duke University, Durham, NC

- Created and instructed course titled "Foundations of an Open Source World"

2010--2015
Instructor
Dept. of Communication Studies, University of North Carolina, Chapel Hill, NC

- Instructed courses in public speaking, media history/theory, and communication theory
- Created and instructed course entitled "Introduction to Video Game Studies"  

2008--2010
Instructor
Dept. of Communication & Theatre, Millersville University of Pennsylvania, Millersville, PA

- Instructed courses in public speaking, communication theory and research, media theory, and persuasion

2007--2008
Instructor

- Dept. of Speech Communication & Theatre, Kutztown University, Kutztown, PA
- Instructed courses in public speaking

Recent Publications
===================

Open Source & Community

- Items published for Red Hat available at <https://redhat.com/en/authors/bryan-behrenshausen>
- Items published for GitLab available at <https://about.gitlab.com/blog/categories/open-source/>

Open Source Culture and Open Organizations

- Opensource.com publication history available at <https://opensource.com/users/bbehrens>
- Contributions to open source community architecture handbook are available at <theopensourceway.org>
- The Open Organization book series is available at <theopenorganization.org/books>

Education
=========

2010--2016
PhD, Communication Studies (Certificate in Cultural Studies)
University of North Carolina, Chapel Hill, NC

2005--2007
M.A., Communication
University of Maine, Orono, ME

2001--2005
B.S., Speech Communication (minor in Print Journalism)
Millersville University of Pennsylvania, Millersville, PA

Committee and Board Service
===========================

2022-present
Board member
Open Way Learning

2021-2022
Advisory board member
The Open Document Foundation

Professional Training
=====================

2020--2021
Open Source Technology Management Program
Brandeis University, Waltham, MA
Courses completed: Open Source Workflow and Infrastructure (2020); Cultivate an Open Source Community (2021)

Skills
======

Open Source Industry & Community Participation

- Broad working knowledge of open source industry, community governance, and business strategy
- Architect, lead, and grow open source communities
- Facilitate and manage relationships between multiple corporate and community stakeholders in open source projects
- Participate in and speak at open source events

Writing & Editing

- Compose clear, concise, engaging, and audience-focused materials in deadline situations
- Prefers docs-as-code approach to documentation projects
- Excels at translating technical concepts and benefits for multiple audiences
- Experience ghostwriting for various personas, including C-level executives

Instructional & Organizational

- Experience teaching learners in a variety of contexts, including online
- Architect informative and persuasive presentations with compelling messages and effective structures
- Enjoys research-oriented approach to projects

Technical Competencies

- Prefers working in open source, Linux-based operating environments
- Manage large documentation projects using Markdown, Git, and popular social coding platforms (GitLab, GitHub, Gitea)
- Design and code W3C standards-compliant websites in HTML and CSS
- Produce publication-ready books, whitepapers, and manuscripts with open source desktop publishing tools
